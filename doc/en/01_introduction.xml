<chapter id="introduction">

  <title>Introduction</title>

<para>
  A general purpose operating system like Debian can be the
  perfect solution for many different problems.  Whether
  you want Debian to work for you in the classroom, as a
  games machine, or in the office, each problem area has its
  own unique needs and requires a different subset of
  packages tailored in a different way.
</para>

<para>
	Debian Pure Blends
	provide support for special user interests.  They implement a new
	approach to cover interests of specialised users, who might be
	children, lawyers, medical staff, visually impaired people, etc.  Of
	late, several Debian Pure Blends have evolved.  The common goal of
	those is to make installation and administration of computers for
	their target users as easy as possible, and to serve in the role as
	the missing link between software developers and users well.
</para>

<para>
	To clarify the relation between a Blend and a derivative which is
	frequently mixed up Ben Armstrong said in a
	<ulink url="http://lists.debian.org/debian-blends/2011/07/msg00010.html">
		discussion on the Blends mailing list</ulink>:
	"While a Blend strives to mainstream with Debian, a derivative
	 strives to differentiate from Debian."
</para>

<para>
	Using the object oriented approach as an analogy, if Debian as a whole
	is an object, a Debian Pure Blend is an instance of this object that
	inherits all features while providing certain properties.
	</para>

<para>
	So the Debian project releases the Debian Distribution which
	includes several Blends.  In contrast to this, there might be some
	other Debian related Projects, either external or non-official, which
	may create "derivative distributions".  But these are not the
	responsibility of the Debian project.
</para>

<para>
	A word of warning:  The fact that a Blend covering a certain field of
	work does exist does not mean that it might be a complete drop in
	replacement of Free Software solutions for all tasks in this specific
	field.  Some Blends just started to work on this and adopted the
	technical framework to formalise the work on the project but it might
	perfectly happen that there is just a lack of available Free Software
	solutions for certain tasks.  Debian can do less about this because we
	just assemble a set of software which was developed outside the Debian
	GNU/Linux distribution.  So it has to be checked whether a specific
	Blend is fit for the intended purpose, whether it might cover just
	some parts of a fields of work or whether it is just a concept to develop
	some solutions for the future.
</para>

</chapter>